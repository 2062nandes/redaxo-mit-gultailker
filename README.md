# REDAXO mit ALPINEJS, TAILWINDCSS, DOCKER, Browserify, PostCSS, Pug, ES6 und Tailpine

## Installation global requeriments
- Npm
- Docker
- Docker Compose
- Gulp

## Project setup
```
cp .env.example .env
```
```
npm install
```
```
docker-compose up -d
```

### Compiles assets and hot-reloads for development
```
gulp
```