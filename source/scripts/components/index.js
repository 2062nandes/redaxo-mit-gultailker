import Vue from 'vue/dist/vue'
import VueTinySlider from 'vue-tiny-slider'

Vue.component('rex-slider', VueTinySlider)
