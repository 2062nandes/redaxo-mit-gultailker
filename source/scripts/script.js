import Vue from 'vue/dist/vue'
import './components'
import '../templates/components/HeaderComponent'
import '../templates/components/FooterComponent'

const rex = new Vue({
  el: '#rex'
})

Vue.use(rex)
