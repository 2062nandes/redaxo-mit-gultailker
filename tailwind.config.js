module.exports = {
  theme: {
    extend: {
      spacing: {
        '80': '20rem',
        '108': '27rem'
      },
      borderWidth: {
        '14': '14px'
      }
    },
    container: {
      padding: '1rem'
    },
    colors: {
      primary: {
        base: 'var(--primary-color)',
        100: 'hsl(var(--primary-HS), 90%)',
        200: 'hsl(var(--primary-HS), 80%)',
        300: 'hsl(var(--primary-HS), 70%)',
        400: 'hsl(var(--primary-HS), 60%)',
        500: 'hsl(var(--primary-HS), 50%)',
        600: 'hsl(var(--primary-HS), 40%)',
        700: 'hsl(var(--primary-HS), 30%)',
        800: 'hsl(var(--primary-HS), 20%)',
        900: 'hsl(var(--primary-HS), 10%)'
      },
      secondary: {
        base: 'var(--secondary-color)',
        100: 'hsl(var(--secondary-HS), 90%)',
        200: 'hsl(var(--secondary-HS), 80%)',
        300: 'hsl(var(--secondary-HS), 70%)',
        400: 'hsl(var(--secondary-HS), 60%)',
        500: 'hsl(var(--secondary-HS), 50%)',
        600: 'hsl(var(--secondary-HS), 40%)',
        700: 'hsl(var(--secondary-HS), 30%)',
        800: 'hsl(var(--secondary-HS), 20%)',
        900: 'hsl(var(--secondary-HS), 10%)'
      },
      accent: {
        base: 'var(--accent-color)',
        100: 'hsl(var(--accent-HS), 90%)',
        200: 'hsl(var(--accent-HS), 80%)',
        300: 'hsl(var(--accent-HS), 70%)',
        400: 'hsl(var(--accent-HS), 60%)',
        500: 'hsl(var(--accent-HS), 50%)',
        600: 'hsl(var(--accent-HS), 40%)',
        700: 'hsl(var(--accent-HS), 30%)',
        800: 'hsl(var(--accent-HS), 20%)',
        900: 'hsl(var(--accent-HS), 10%)'
      },
      'accent-2': {
        base: 'var(--accent-2-color)',
        100: 'hsl(var(--accent-2-HS), 90%)',
        200: 'hsl(var(--accent-2-HS), 80%)',
        300: 'hsl(var(--accent-2-HS), 70%)',
        400: 'hsl(var(--accent-2-HS), 60%)',
        500: 'hsl(var(--accent-2-HS), 50%)',
        600: 'hsl(var(--accent-2-HS), 40%)',
        700: 'hsl(var(--accent-2-HS), 30%)',
        800: 'hsl(var(--accent-2-HS), 20%)',
        900: 'hsl(var(--accent-2-HS), 10%)'
      },
      error: {
        base: 'var(--error-color)',
        100: 'hsl(var(--error-HS), 90%)',
        200: 'hsl(var(--error-HS), 80%)',
        300: 'hsl(var(--error-HS), 70%)',
        400: 'hsl(var(--error-HS), 60%)',
        500: 'hsl(var(--error-HS), 50%)',
        600: 'hsl(var(--error-HS), 40%)',
        700: 'hsl(var(--error-HS), 30%)',
        800: 'hsl(var(--error-HS), 20%)',
        900: 'hsl(var(--error-HS), 10%)'
      },
      warning: {
        base: 'var(--warning-color)',
        100: 'hsl(var(--warning-HS), 90%)',
        200: 'hsl(var(--warning-HS), 80%)',
        300: 'hsl(var(--warning-HS), 70%)',
        400: 'hsl(var(--warning-HS), 60%)',
        500: 'hsl(var(--warning-HS), 50%)',
        600: 'hsl(var(--warning-HS), 40%)',
        700: 'hsl(var(--warning-HS), 30%)',
        800: 'hsl(var(--warning-HS), 20%)',
        900: 'hsl(var(--warning-HS), 10%)',
      },
      success: {
        base: 'var(--success-color)',
        100: 'hsl(var(--success-HS), 90%)',
        200: 'hsl(var(--success-HS), 80%)',
        300: 'hsl(var(--success-HS), 70%)',
        400: 'hsl(var(--success-HS), 60%)',
        500: 'hsl(var(--success-HS), 50%)',
        600: 'hsl(var(--success-HS), 40%)',
        700: 'hsl(var(--success-HS), 30%)',
        800: 'hsl(var(--success-HS), 20%)',
        900: 'hsl(var(--success-HS), 10%)',
      },
      info: {
        base: 'var(--info-color)',
        100: 'hsl(var(--info-HS), 90%)',
        200: 'hsl(var(--info-HS), 80%)',
        300: 'hsl(var(--info-HS), 70%)',
        400: 'hsl(var(--info-HS), 60%)',
        500: 'hsl(var(--info-HS), 50%)',
        600: 'hsl(var(--info-HS), 40%)',
        700: 'hsl(var(--info-HS), 30%)',
        800: 'hsl(var(--info-HS), 20%)',
        900: 'hsl(var(--info-HS), 10%)'
      }
    },
    fontFamily: {
      sans: [
        'Nunito Sans',
        'Roboto',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ['Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: [
        'Menlo',
        'Monaco',
        'Consolas',
        '"Liberation Mono"',
        '"Courier New"',
        'monospace'
      ]
    }
  },
  variants: {
    // Some useful comment
  },
  plugins: [
    // Some useful comment
  ]
}
